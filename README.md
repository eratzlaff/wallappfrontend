#Wall App Frontend

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-black.svg)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappfrontend)

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=eratzlaff_wallappfrontend&metric=alert_status)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappfrontend)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=eratzlaff_wallappfrontend&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappfrontend)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=eratzlaff_wallappfrontend&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappfrontend)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=eratzlaff_wallappfrontend&metric=security_rating)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappfrontend)

[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=eratzlaff_wallappfrontend&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappfrontend)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=eratzlaff_wallappfrontend&metric=bugs)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappfrontend)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=eratzlaff_wallappfrontend&metric=code_smells)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappfrontend)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=eratzlaff_wallappfrontend&metric=sqale_index)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappfrontend)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=eratzlaff_wallappfrontend&metric=ncloc)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappfrontend)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=eratzlaff_wallappfrontend&metric=coverage)](https://sonarcloud.io/dashboard?id=eratzlaff_wallappfrontend)

## Getting Started

### Development Prerequisites
* nodejs 12
* yarn
* [Wall App Backend](https://bitbucket.org/eratzlaff/wallappbackend/)

Install dependencies

> If you are using `nvm` switch to nodejs version 12 `nvm use 12`

```bash
yarn install 
```
Set the environment variables
````shell
cp ./.env.local.example ./.env.local
````
> Open and edit the `.env.local` file and change de Environment Variable to the development value, the default value are most of the time ok.

Run the development server:

```bash
yarn run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

### Container Prerequisites


In order to run this container you'll need docker installed.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

### Usage

#### Container Parameters

Environment Variable
```dotenv
NEXT_PUBLIC_API=http://<BACKEND IP ADDRESS>:8000
```

> Replace `<BACKEEND IP ADDRESS>` with a valid URL

Building the container

```shell
docker build --no-cache --rm --tag=wallappfrontend .  
```

> Change the tag to a valid registry tag if you want to publish the container image on a registry

Run the continer `Example values`

```shell
docker run -p 3000:3000 \
-e NEXT_PUBLIC_API=http://<BACKEND IP ADDRESS>:8000  \
wallappfrontend
```
> Replace `<BACKEEND IP ADDRESS>` with a valid URL

## Authors

* **Elvis Ratzlaff** - *Initial work*


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
