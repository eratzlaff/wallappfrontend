import React from "react";
import { CssBaseline } from "@material-ui/core";
import WallList from "../features/wall/WallList";
import SSR from "../src/baseSSR";

export default function Index(props) {
  return (
    <React.Fragment>
      <CssBaseline />
      <WallList />
    </React.Fragment>
  );
}

export async function getServerSideProps(context) {
  return SSR(context);
}
