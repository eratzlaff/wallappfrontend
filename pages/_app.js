import React from "react";
import PropTypes from "prop-types";
import Head from "next/head";
import { ThemeProvider } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import theme from "../src/theme";
import { Provider } from "react-redux";
import { refreshTokenRequest } from "../features/token/tokenSlice";
import Layout from "../features/Layout";
import getStore from "../app/store";

export default function MyApp({ Component, pageProps }) {
  const store = getStore(pageProps.initialReduxState);

  React.useEffect(() => {
    setInterval(() => {
      if (store.getState().token.profile !== null) {
        let currentDate = new Date();
        let expDate = new Date(store.getState().token.profile.exp * 1000);
        if (currentDate.getTime() + 1000 * 60 > expDate.getTime()) {
          store.dispatch(refreshTokenRequest());
        }
      }
    }, 3000);

    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  return (
    <Provider store={store}>
      <React.Fragment>
        <Head>
          <title>Wall App</title>
          <meta
            name="viewport"
            content="minimum-scale=1, initial-scale=1, width=device-width"
          />
        </Head>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <Layout />
          <Component {...pageProps} />
        </ThemeProvider>
      </React.Fragment>
    </Provider>
  );
}

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object.isRequired,
};
