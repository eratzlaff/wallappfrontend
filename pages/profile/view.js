import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import { TextField } from "mui-rff";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Link from "../../src/Link";
import { useDispatch, useSelector } from "react-redux";
import { selectLoggedIn, selectProfile } from "../../features/token/tokenSlice";
import {
  loadUserRequest,
  selectUserProfile,
  selectUsers,
} from "../../features/user/userSlice";
import { Form } from "react-final-form";
import { useWallStyles } from "../../src/useStyles";
import SSR from "../../src/baseSSR";

export default function SignUp() {
  const profile = useSelector(selectProfile);
  const users = useSelector(selectUsers);
  const loggedIn = useSelector(selectLoggedIn);
  const user = useSelector(selectUserProfile(profile));
  const dispatch = useDispatch();
  const classes = useWallStyles();

  React.useEffect(() => {
    if (!users.loading && !users.loaded) {
      dispatch(loadUserRequest());
    }
  }, [profile, loggedIn]);

  return (
    <Container component="main" maxWidth="sm">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Profile
        </Typography>
        <Form
          onSubmit={() => null}
          initialValues={user}
          render={({ handleSubmit, form, submitting, pristine, values }) => (
            <form onSubmit={handleSubmit} noValidate className={classes.form}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    disabled
                    fullWidth
                    name="username"
                    label="Username"
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    variant="outlined"
                    disabled
                    fullWidth
                    name="first_name"
                    label="First Name"
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    variant="outlined"
                    disabled
                    fullWidth
                    name="last_name"
                    label="Last Name"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    disabled
                    fullWidth
                    name="email"
                    label="Email Address"
                  />
                </Grid>
              </Grid>
              <Button
                href="/profile/edit"
                component={Link}
                type="button"
                fullWidth
                variant="outlined"
                color="secondary"
                className={classes.submit}
              >
                Edit
              </Button>
              <Button
                href="/"
                component={Link}
                type="button"
                fullWidth
                variant="text"
                className={classes.home}
              >
                Home Page
              </Button>
            </form>
          )}
        />
      </div>
    </Container>
  );
}

export async function getServerSideProps(context) {
  return SSR(context);
}
