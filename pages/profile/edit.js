import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import { TextField } from "mui-rff";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Link from "../../src/Link";
import { useDispatch, useSelector } from "react-redux";
import { selectProfile } from "../../features/token/tokenSlice";
import {
  loadUserRequest,
  selectUserErrors,
  selectUserProfile,
  selectUsers,
  updateUserRequest,
} from "../../features/user/userSlice";
import { Form } from "react-final-form";
import { useWallStyles } from "../../src/useStyles";
import SSR from "../../src/baseSSR";

export default function SignUp() {
  const emailRegEx =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,2|3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  const profile = useSelector(selectProfile);
  const users = useSelector(selectUsers);
  const userError = useSelector(selectUserErrors);
  const user = useSelector(selectUserProfile(profile));
  const dispatch = useDispatch();
  const classes = useWallStyles();

  React.useEffect(() => {
    if (!users.loading && !users.loaded) {
      dispatch(loadUserRequest());
    }
  }, [users]);

  const onSubmit = async (values) => {
    dispatch(updateUserRequest(values));
  };

  const verifyBackend = (values) => {
    if (userError !== null) {
      return userError;
    } else {
      return {};
    }
  };

  const validate = (values) => {
    const errors = {};
    if (!values.username) {
      errors.username = "Required";
    }
    if (!values.last_name) {
      errors.last_name = "Required";
    }
    if (!values.first_name) {
      errors.first_name = "Required";
    }
    if (!emailRegEx.exec(values.email)) {
      errors.email = "Not a valid email address";
    }
    if (!values.email) {
      errors.email = "Required";
    }
    if (!values.password && values.confirm) {
      errors.password = "Required";
    }
    if (!values.confirm && values.password) {
      errors.confirm = "Required";
    } else if (values.confirm !== values.password) {
      errors.confirm = "Must match";
    }
    return Object.keys(errors).length ? errors : verifyBackend(values);
  };

  return (
    <Container component="main" maxWidth="sm">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Profile
        </Typography>
        <Form
          onSubmit={onSubmit}
          initialValues={user}
          validate={validate}
          render={({ handleSubmit, form, submitting, pristine, values }) => (
            <form onSubmit={handleSubmit} noValidate className={classes.form}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    fullWidth
                    disabled
                    name="username"
                    label="Username"
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    variant="outlined"
                    fullWidth
                    name="first_name"
                    label="First Name"
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    variant="outlined"
                    fullWidth
                    name="last_name"
                    label="Last Name"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    fullWidth
                    name="email"
                    label="Email Address"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    autoComplete="current-password"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    fullWidth
                    name="confirm"
                    label="Confirm"
                    type="password"
                    autoComplete="current-confirm"
                  />
                </Grid>
              </Grid>
              <Button
                type="submit"
                fullWidth
                variant="outlined"
                color="secondary"
                disabled={submitting}
                className={classes.submit}
              >
                Save
              </Button>
              <Button
                type="button"
                href="/profile/view"
                fullWidth
                component={Link}
                variant="text"
                className={classes.home}
              >
                Cancel
              </Button>
            </form>
          )}
        />
      </div>
    </Container>
  );
}

export async function getServerSideProps(context) {
  return SSR(context);
}
