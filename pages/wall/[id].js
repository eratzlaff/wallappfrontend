import React from "react";
import { useDispatch, useSelector } from "react-redux";
import Button from "@material-ui/core/Button";
import Link from "../../src/Link";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import { TextField } from "mui-rff";
import Container from "@material-ui/core/Container";
import {
  loadWallRequest,
  selectWall,
  selectWallErrors,
  selectWalls,
  updateWallRequest,
} from "../../features/wall/wallSlice";
import { useRouter } from "next/router";
import { Form } from "react-final-form";
import { useWallStyles } from "../../src/useStyles";
import SSR from "../../src/baseSSR";
import { Alert } from "@material-ui/lab";

export default function Edit() {
  const router = useRouter();
  const walls = useSelector(selectWalls);
  const wallError = useSelector(selectWallErrors);
  const wall = useSelector(selectWall(router.query.id));
  const classes = useWallStyles();
  const dispatch = useDispatch();

  React.useEffect(() => {
    if (!walls.loading && !walls.loaded) {
      dispatch(loadWallRequest());
    }
  }, [wall, walls]);

  const onSubmit = async (values) => {
    dispatch(updateWallRequest(values));
  };

  const verifyBackend = (values) => {
    if (wallError !== null) {
      return wallError;
    } else {
      return {};
    }
  };

  const validate = (values) => {
    const errors = {};
    if (!values.subject) {
      errors.subject = "Required";
    }
    if (!values.message) {
      errors.message = "Required";
    }
    return Object.keys(errors).length ? errors : verifyBackend(values);
  };

  return (
    <Container component="main" maxWidth="sm">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          New Wall Message
        </Typography>
        <Form
          onSubmit={onSubmit}
          initialValues={wall}
          validate={validate}
          render={({ handleSubmit, form, submitting, pristine, values }) => (
            <form onSubmit={handleSubmit} noValidate className={classes.form}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                label="Subject"
                name="subject"
                autoComplete="subject"
                autoFocus
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                multiline
                rows="5"
                label="Message"
                name="message"
                autoComplete="message"
              />
              <Button
                type="submit"
                fullWidth
                variant="outlined"
                color="secondary"
                disabled={submitting}
                className={classes.submit}
              >
                Update
              </Button>
              <Button
                href="/"
                component={Link}
                type="button"
                fullWidth
                variant="text"
                className={classes.home}
              >
                Home Page
              </Button>
            </form>
          )}
        />
        {wallError ? <Alert severity="error">{wallError.detail}</Alert> : <></>}
      </div>
    </Container>
  );
}

export async function getServerSideProps(context) {
  return SSR(context);
}
