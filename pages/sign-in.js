import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import { TextField } from "mui-rff";
import Link from "../src/Link";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import { useDispatch, useSelector } from "react-redux";
import { loginRequest, selectTokenErrors } from "../features/token/tokenSlice";
import { Form } from "react-final-form";
import { useWallStyles } from "../src/useStyles";
import SSR from "../src/baseSSR";
import { Alert } from "@material-ui/lab";

export default function SignIn() {
  const userError = useSelector(selectTokenErrors);
  const dispatch = useDispatch();

  const classes = useWallStyles();

  const onSubmit = async (values) => {
    dispatch(loginRequest(values));
  };

  const verifyBackend = (values) => {
    if (userError !== null) {
      return userError;
    } else {
      return {};
    }
  };

  const validate = (values) => {
    const errors = {};
    if (!values.username) {
      errors.username = "Required";
    }
    if (!values.password) {
      errors.password = "Required";
    }
    return Object.keys(errors).length ? errors : verifyBackend(values);
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <Form
          onSubmit={onSubmit}
          initialValues={{}}
          validate={validate}
          render={({ handleSubmit, form, submitting, pristine, values }) => (
            <form onSubmit={handleSubmit} noValidate className={classes.form}>
              <TextField
                variant="outlined"
                margin="normal"
                required={true}
                fullWidth
                label="Username"
                name="username"
                autoComplete="username"
                autoFocus
              />
              <TextField
                variant="outlined"
                margin="normal"
                required={true}
                fullWidth
                name="password"
                label="Password"
                type="password"
                autoComplete="current-password"
              />
              <Button
                type="submit"
                fullWidth
                variant="outlined"
                color="secondary"
                disabled={submitting}
                className={classes.submit}
              >
                Sign In
              </Button>
              <Button
                href="/"
                component={Link}
                type="button"
                fullWidth
                variant="text"
                color="secondary"
                className={classes.home}
              >
                Home Page
              </Button>
              <Grid container justify="flex-end"></Grid>
              <Grid item>
                <Link href="/sign-up" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </form>
          )}
        />
        {userError ? <Alert severity="error">{userError.detail}</Alert> : <></>}
      </div>
    </Container>
  );
}

export async function getServerSideProps(context) {
  return SSR(context);
}
