import axios from "axios";
import { loadUser } from "../features/user/userSlice";
import { loadWall } from "../features/wall/wallSlice";
import { getAppCookies } from "./utils";
import { login, logout } from "../features/token/tokenSlice";
import getStore from "../app/store";
import { setAPI } from "../features/config/configSlice";

export default async function SSR(context) {
  const api = process.env.NEXT_PUBLIC_API;
  const reduxStore = getStore({});
  const { dispatch } = reduxStore;
  const { req } = context;
  dispatch(setAPI(api));
  await axios.get(api + "/api/user/").then((response) => {
    dispatch(loadUser(response.data));
  });

  await axios.get(api + "/api/wall/").then((response) => {
    dispatch(loadWall(response.data));
  });
  const { refresh, access } = getAppCookies(req);
  if (refresh !== undefined && access !== undefined) {
    dispatch(login({ access, refresh }));
  } else {
    dispatch(logout());
  }
  return { props: { initialReduxState: reduxStore.getState(), api } };
}
