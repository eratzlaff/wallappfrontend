import { createMuiTheme } from "@material-ui/core/styles";

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    type: "dark",
    primary: {
      main: "#0f4c75",
    },
    secondary: {
      main: "#3282b8",
    },
    error: {
      main: "#E44C65",
    },
    background: {
      default: "#1b262c",
    },
  },
});

export default theme;
