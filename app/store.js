import { configureStore } from "@reduxjs/toolkit";
import tokenReducer from "../features/token/tokenSlice";
import wallReducer from "../features/wall/wallSlice";
import userReducer from "../features/user/userSlice";
import configReducer from "../features/config/configSlice";
import logger from "redux-logger";
import { createRouterMiddleware, routerReducer } from "connected-next-router";

const routerMiddleware = createRouterMiddleware();

export default function getStore(initialState) {
  const store = configureStore({
    reducer: {
      router: routerReducer,
      token: tokenReducer,
      wall: wallReducer,
      user: userReducer,
      config: configReducer,
    },
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(routerMiddleware).concat(logger),
    preloadedState: initialState,
  });

  return store;
}
