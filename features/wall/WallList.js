import { useDispatch, useSelector } from "react-redux";
import { selectLoggedIn } from "../token/tokenSlice";
import React from "react";
import {
  loadWallRequest,
  nextWallRequest,
  previousWallRequest,
  selectWalls,
} from "./wallSlice";
import Wall from "./Wall";
import { Button } from "@material-ui/core";
import Container from "@material-ui/core/Container";
import Link from "../../src/Link";
import { useRouter } from "next/router";

export default function WallList() {
  const walls = useSelector(selectWalls);
  const loggedIn = useSelector(selectLoggedIn);
  const dispatch = useDispatch();

  React.useEffect(() => {
    if (!walls.loading && !walls.loaded) {
      dispatch(loadWallRequest());
    }
  }, [walls, loggedIn]);

  const next = () => {
    dispatch(nextWallRequest());
  };

  const previous = () => {
    dispatch(previousWallRequest());
  };

  const reload = () => {
    dispatch(loadWallRequest());
  };

  const addMessage = () => {
    if (loggedIn) {
      return (
        <>
          <Button
            color="secondary"
            href="/wall/new"
            component={Link}
            variant="outlined"
          >
            Add Message
          </Button>
          <Button color="secondary" variant="outlined" onClick={() => reload()}>
            Reload
          </Button>
        </>
      );
    } else {
      return (
        <Button color="secondary" variant="outlined" onClick={() => reload()}>
          Reload
        </Button>
      );
    }
  };

  const paginate = () => {
    return (
      <>
        <Button disabled={walls.previous === null} onClick={previous}>
          Previews
        </Button>
        <Button disabled={walls.next === null} onClick={next}>
          Next
        </Button>
      </>
    );
  };

  return (
    <Container component="main" maxWidth="sm">
      {addMessage()}
      {paginate()}
      {walls.results.map((wall) => (
        <Wall key={wall.id} wall={wall} />
      ))}
    </Container>
  );
}
