import { createSelector, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { push } from "connected-next-router";

const initialState = {
  loaded: false,
  loading: false,
  updateRunning: false,
  removeRunning: false,
  createRunning: false,
  next: null,
  previous: null,
  results: [],
  error: null,
};

export const wallSlice = createSlice({
  name: "wall",
  initialState,
  reducers: {
    loadWall: (state, action) => {
      state.loaded = true;
      state.loading = false;
      state.next = action.payload.next;
      state.previous = action.payload.previous;
      state.results = action.payload.results;
    },
    loadWallStart: (state) => {
      state.loading = true;
    },
    loadWallEnd: (state) => {
      state.loading = false;
    },
    updateWallStart: (state) => {
      state.updateRunning = true;
    },
    updateWallEnd: (state) => {
      state.updateRunning = false;
    },
    removeWallStart: (state) => {
      state.removeRunning = true;
    },
    removeWallEnd: (state) => {
      state.removeRunning = false;
    },
    createWallStart: (state) => {
      state.createRunning = true;
    },
    createWallEnd: (state) => {
      state.createRunning = false;
    },
    setWallErrors: (state, action) => {
      state.errors = action.payload;
    },
    removeWallErrors: (state) => {
      state.errors = null;
    },
  },
});

export const {
  loadWall,
  loadWallStart,
  loadWallEnd,
  updateWallStart,
  updateWallEnd,
  removeWallStart,
  removeWallEnd,
  createWallStart,
  createWallEnd,
  setWallErrors,
  removeWallErrors,
} = wallSlice.actions;

export const loadWallRequest = () => (dispatch, getState) => {
  dispatch(loadWallStart());
  const state = getState();
  const api = state.config.api;
  axios
    .get(api + "/api/wall/")
    .then((response) => {
      dispatch(loadWall(response.data));
    })
    .catch((error) => {
      dispatch(setWallErrors(error.response.data));
      dispatch(loadWallEnd());
      dispatch(removeWallErrorsAsync());
    });
};

export const nextWallRequest = () => (dispatch, getState) => {
  const state = getState();
  dispatch(loadWallStart());
  axios
    .get(state.wall.next)
    .then((response) => {
      dispatch(loadWall(response.data));
    })
    .catch((error) => {
      try {
        dispatch(setWallErrors(error.response.data));
      } catch (e) {
        dispatch(setWallErrors({ detail: "ERROR" }));
      }
      dispatch(loadWallEnd());
      dispatch(removeWallErrorsAsync());
    });
};

export const previousWallRequest = () => (dispatch, getState) => {
  const state = getState();
  dispatch(loadWallStart());
  axios
    .get(state.wall.previous)
    .then((response) => {
      dispatch(loadWall(response.data));
    })
    .catch((error) => {
      try {
        dispatch(setWallErrors(error.response.data));
      } catch (e) {
        dispatch(setWallErrors({ detail: "ERROR" }));
      }
      dispatch(loadWallEnd());
      dispatch(removeWallErrorsAsync());
    });
};

export const createWallRequest = (wall) => (dispatch, getState) => {
  const state = getState();
  const api = state.config.api;
  dispatch(createWallStart());
  axios
    .post(api + "/api/wall/", wall, {
      headers: {
        Authorization: `Bearer ` + state.token.access,
      },
    })
    .then((response) => {
      dispatch(loadWallRequest());
      dispatch(createWallEnd());
      dispatch(push("/"));
    })
    .catch((error) => {
      try {
        dispatch(setWallErrors(error.response.data));
      } catch (e) {
        dispatch(setWallErrors({ detail: "ERROR" }));
      }
      dispatch(createWallEnd());
      dispatch(removeWallErrorsAsync());
    });
};

export const removeWallRequest = (wall) => (dispatch, getState) => {
  const state = getState();
  const api = state.config.api;
  dispatch(removeWallStart());
  axios
    .delete(api + "/api/wall/" + wall.id + "/", {
      headers: {
        Authorization: `Bearer ` + state.token.access,
      },
    })
    .then((response) => {
      dispatch(loadWallRequest());
      dispatch(removeWallEnd());
      dispatch(push("/"));
    })
    .catch((error) => {
      try {
        dispatch(setWallErrors(error.response.data));
      } catch (e) {
        dispatch(setWallErrors({ detail: "ERROR" }));
      }
      dispatch(removeWallEnd());
      dispatch(removeWallErrorsAsync());
    });
};

export const updateWallRequest = (wall) => (dispatch, getState) => {
  const state = getState();
  const api = state.config.api;
  dispatch(updateWallStart());
  axios
    .put(api + "/api/wall/" + wall.id + "/", wall, {
      headers: {
        Authorization: `Bearer ` + state.token.access,
      },
    })
    .then((response) => {
      dispatch(loadWallRequest());
      dispatch(updateWallEnd());
      dispatch(push("/"));
    })
    .catch((error) => {
      try {
        dispatch(setWallErrors(error.response.data));
      } catch (e) {
        dispatch(setWallErrors({ detail: "ERROR" }));
      }
      dispatch(updateWallEnd());
      dispatch(removeWallErrorsAsync());
    });
};

export const removeWallErrorsAsync = () => (dispatch) => {
  setTimeout(() => {
    dispatch(removeWallErrors());
  }, 5 * 1000);
};

// SELECTORS
export const selectWalls = (state) => state.wall;
export const selectWallErrors = (state) => state.wall.errors;

export const selectWall = (wallId) =>
  createSelector(
    (state) => {
      if (state.wall !== undefined && state.wall.results !== undefined) {
        if (state.wall.loaded) {
          return state.wall.results.filter(
            (wall) => wall.id === parseInt(wallId)
          );
        }
      }
      return [{}];
    },
    (walls) => walls[0]
  );

export default wallSlice.reducer;
