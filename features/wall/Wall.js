import { useDispatch, useSelector } from "react-redux";
import { selectProfile } from "../token/tokenSlice";
import React from "react";
import { removeWallRequest } from "./wallSlice";
import {
  Card,
  CardActions,
  CardContent,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import Link from "../../src/Link";
import UserName from "../user/UserName";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  card: {
    marginTop: 8,
  },
  remove: {
    color: "red",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export default function Wall(props) {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();
  const { wall } = props;

  const profile = useSelector(selectProfile);
  const dispatch = useDispatch();

  const handleRemove = () => {
    dispatch(removeWallRequest(wall));
  };

  const handleConfirmationOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const cardAcctions = (wall) => {
    if (profile && profile.username === wall.owner) {
      return (
        <CardActions>
          <Button
            size="small"
            color="secondary"
            variant="outlined"
            href={"/wall/" + wall.id}
            component={Link}
          >
            Edit
          </Button>
          <Button size="small" variant="text" onClick={handleConfirmationOpen}>
            Remove
          </Button>
        </CardActions>
      );
    }
  };

  return (
    <React.Fragment>
      <Card key={wall.id} className={classes.card}>
        <CardContent>
          <Typography variant="h5" component="h2">
            {wall.subject}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="pre">
            {wall.message}
          </Typography>
          <Typography
            className={classes.pos}
            color="textSecondary"
            align="right"
            component="div"
          >
            <UserName username={wall.owner} />
          </Typography>
        </CardContent>
        {cardAcctions(wall)}
      </Card>
      <Dialog
        open={open}
        fullWidth={true}
        maxWidth="xs"
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Are you sure?"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            You won&apos;t be able to revert this!
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary" variant="outlined">
            No
          </Button>
          <Button onClick={handleRemove} variant="text" autoFocus>
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}
