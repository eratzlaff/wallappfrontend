import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadUserRequest, selectUser, selectUsers } from "./userSlice";

export default function UserName(props) {
  const { username } = props;
  const users = useSelector(selectUsers);
  const user = useSelector(selectUser(username));
  const dispatch = useDispatch();

  React.useEffect(() => {
    if (!users.loading && !users.loaded) {
      dispatch(loadUserRequest());
    }
  }, [users]);

  return (
    <React.Fragment>
      {user.first_name} {user.last_name}
    </React.Fragment>
  );
}
