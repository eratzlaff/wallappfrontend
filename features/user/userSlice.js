import { createSelector, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { push } from "connected-next-router";

const initialState = {
  loaded: false,
  loading: false,
  updateRunning: false,
  results: [],
  errors: null,
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    loadUser: (state, action) => {
      state.loaded = true;
      state.loading = false;
      state.results = action.payload;
    },
    loadUserStart: (state) => {
      state.loading = true;
    },
    loadUserEnd: (state) => {
      state.loading = false;
    },
    updateUserStart: (state) => {
      state.updateRunning = true;
    },
    updateUserEnd: (state) => {
      state.updateRunning = false;
    },
    setUserErrors: (state, action) => {
      state.errors = action.payload;
    },
    removeUserErrors: (state) => {
      state.errors = null;
    },
  },
});

export const {
  loadUser,
  loadUserStart,
  loadUserEnd,
  updateUserStart,
  updateUserEnd,
  setUserErrors,
  removeUserErrors,
} = userSlice.actions;

export const loadUserRequest = () => (dispatch, getState) => {
  dispatch(loadUserStart());
  const api = getState().config.api;
  axios
    .get(api + "/api/user/")
    .then((response) => {
      dispatch(loadUser(response.data));
    })
    .catch((error) => {
      try {
        dispatch(setUserErrors(error.response.data));
      } catch (e) {
        dispatch(setUserErrors({ detail: "ERROR" }));
      }
      dispatch(loadUserEnd());
      dispatch(removeUserErrorsAsync());
    });
};

export const updateUserRequest = (user) => (dispatch, getState) => {
  const state = getState();
  const api = state.config.api;
  dispatch(updateUserStart());
  axios
    .put(api + "/api/user/" + user.id + "/", user, {
      headers: {
        Authorization: `Bearer ` + state.token.access,
      },
    })
    .then((response) => {
      dispatch(loadUserRequest());
      dispatch(push("/profile/view"));
      dispatch(updateUserEnd());
    })
    .catch((error) => {
      try {
        dispatch(setUserErrors(error.response.data));
      } catch (e) {
        dispatch(setUserErrors({ detail: "ERROR" }));
      }
      dispatch(updateUserEnd());
      dispatch(removeUserErrorsAsync());
    });
};

export const removeUserErrorsAsync = () => (dispatch) => {
  setTimeout(() => {
    dispatch(removeUserErrors());
  }, 5 * 1000);
};

// SELECTORS
export const selectUsers = (state) => state.user;
export const selectUserErrors = (state) => state.user.errors;

export const selectUser = (username) =>
  createSelector(
    (state) => {
      if (state.user !== undefined && state.user.results !== undefined) {
        if (state.user.loaded) {
          return state.user.results.filter(
            (user) => user.username === username
          );
        }
      }
      return [{}];
    },
    (users) => users[0]
  );

export const selectUserProfile = (profile) =>
  createSelector(
    (state) => {
      if (profile !== null)
        if (state.user !== undefined && state.user.results !== undefined) {
          if (state.user.loaded) {
            return state.user.results.filter(
              (user) => user.username === profile.username
            );
          }
        }
      return [{}];
    },
    (users) => users[0]
  );

export default userSlice.reducer;
