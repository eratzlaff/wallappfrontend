import { createSlice } from "@reduxjs/toolkit";
import Cookies from "js-cookie";
import axios from "axios";
import atob from "atob";
import { push } from "connected-next-router";

const initialState = {
  access: "",
  refresh: "",
  loggedIn: false,
  profile: null,
  errors: null,
};

export const tokenSlice = createSlice({
  name: "token",
  initialState,
  reducers: {
    login: (state, action) => {
      state.access = action.payload.access;
      state.refresh = action.payload.refresh;
      state.loggedIn = true;
      state.profile = JSON.parse(atob(state.access.split(".")[1]));
      Cookies.set("access", state.access);
      Cookies.set("refresh", state.refresh);
    },
    logout: (state) => {
      state.access = "";
      state.refresh = "";
      state.loggedIn = false;
      state.profile = null;
      Cookies.remove("access");
      Cookies.remove("refresh");
    },
    refreshToken: (state, action) => {
      state.access = action.payload.access;
      state.refresh = action.payload.refresh;
      state.profile = JSON.parse(atob(state.access.split(".")[1]));
      Cookies.set("access", state.access);
      Cookies.set("refresh", state.refresh);
    },
    setTokenErrors: (state, action) => {
      state.errors = action.payload;
    },
    removeTokenErrors: (state) => {
      state.errors = null;
    },
  },
});

export const {
  login,
  logout,
  refreshToken,
  setTokenErrors,
  removeTokenErrors,
} = tokenSlice.actions;

export const loginRequest = (state) => (dispatch, getState) => {
  const api = getState().config.api;
  axios
    .post(api + "/api/token/", state)
    .then((response) => {
      dispatch(login(response.data));
      dispatch(push("/"));
    })
    .catch((error) => {
      try {
        dispatch(setTokenErrors(error.response.data));
      } catch (e) {
        dispatch(setTokenErrors({ detail: "ERROR" }));
      }
      dispatch(removeTokenErrorsAsync());
    });
};

export const signUpRequest = (user) => (dispatch, getState) => {
  const api = getState().config.api;
  axios
    .post(api + "/api/user/", user)
    .then((response) => {
      dispatch(push("/sign-in"));
    })
    .catch((error) => {
      try {
        dispatch(setTokenErrors(error.response.data));
      } catch (e) {
        dispatch(setTokenErrors({ detail: "ERROR" }));
      }
      dispatch(removeTokenErrorsAsync());
    });
};

export const refreshTokenRequest = (state) => (dispatch, getState) => {
  const api = getState().config.api;
  axios
    .post(api + "/api/token/refresh/", {
      refresh: getState().token.refresh,
    })
    .then((response) => {
      dispatch(refreshToken(response.data));
    })
    .catch((error) => {
      try {
        dispatch(setTokenErrors(error.response.data));
      } catch (e) {
        dispatch(setTokenErrors({ detail: "ERROR" }));
      }
      dispatch(removeTokenErrorsAsync());
    });
};

export const removeTokenErrorsAsync = () => (dispatch) => {
  setTimeout(() => {
    dispatch(removeTokenErrors());
  }, 5 * 1000);
};

// SELECTORS
export const selectToken = (state) => state.token;
export const selectLoggedIn = (state) => state.token.loggedIn;
export const selectProfile = (state) => state.token.profile;
export const selectTokenErrors = (state) => state.token.errors;

export default tokenSlice.reducer;
