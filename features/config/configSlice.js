import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  api: "",
};

export const configSlice = createSlice({
  name: "config",
  initialState,
  reducers: {
    setAPI: (state, action) => {
      state.api = action.payload;
    },
  },
});

export const { setAPI } = configSlice.actions;

export const selectConfig = (state) => state.config;

export default configSlice.reducer;
