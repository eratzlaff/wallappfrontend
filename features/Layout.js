import { AppBar, Button, IconButton, Toolbar } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Link from "../src/Link";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { logout, selectLoggedIn } from "./token/tokenSlice";
import MenuIcon from "@material-ui/icons/Menu";
import { useWallStyles } from "../src/useStyles";
import { useRouter } from "next/router";

export default function Layout() {
  const classes = useWallStyles();
  const loggedIn = useSelector(selectLoggedIn);
  const dispatch = useDispatch();
  const router = useRouter();

  const logoutNow = () => {
    dispatch(logout());
    router.push("/");
  };

  return (
    <AppBar
      position="static"
      color="default"
      elevation={0}
      className={classes.appBar}
    >
      <Toolbar className={classes.toolbar}>
        <IconButton
          edge="start"
          href="/"
          component={Link}
          className={classes.menuButton}
          color="inherit"
          aria-label="menu"
        >
          <MenuIcon />
        </IconButton>
        <Typography
          variant="h6"
          color="inherit"
          noWrap
          className={classes.toolbarTitle}
        >
          Wall App
        </Typography>
        {!loggedIn ? (
          <>
            <Button
              href="/sign-up"
              component={Link}
              color="secondary"
              variant="text"
              className={classes.link}
            >
              Sign up
            </Button>
            <Button
              href="/sign-in"
              component={Link}
              color="secondary"
              variant="outlined"
              className={classes.link}
            >
              Sign in
            </Button>
          </>
        ) : (
          <>
            <Button
              href="/profile/view"
              component={Link}
              color="secondary"
              variant="text"
              className={classes.link}
            >
              Profile
            </Button>
            <Button
              onClick={logoutNow}
              color="secondary"
              variant="outlined"
              className={classes.link}
            >
              Logout
            </Button>
          </>
        )}
      </Toolbar>
    </AppBar>
  );
}
